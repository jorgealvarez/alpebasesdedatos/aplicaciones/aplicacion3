<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "clientes".
 *
 * @property int $cod
 * @property string|null $nombre
 * @property int $idcochealquilado
 * @property string|null $Fechaalquiler
 *
 * @property Coches $idcochealquilado0
 */
class Clientes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clientes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idcochealquilado'], 'required'],
            [['idcochealquilado'], 'integer'],
            [['Fechaalquiler'], 'safe'],
            [['nombre'], 'string', 'max' => 50],
            [['idcochealquilado'], 'unique'],
            [['idcochealquilado'], 'exist', 'skipOnError' => true, 'targetClass' => Coches::className(), 'targetAttribute' => ['idcochealquilado' => 'ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod' => 'Cod',
            'nombre' => 'Nombre',
            'idcochealquilado' => 'Idcochealquilado',
            'Fechaalquiler' => 'Fechaalquiler',
        ];
    }

    /**
     * Gets query for [[Idcochealquilado0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdcochealquilado0()
    {
        return $this->hasOne(Coches::className(), ['ID' => 'idcochealquilado']);
    }
}
